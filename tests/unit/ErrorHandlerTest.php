<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 10/24/18
 * Time: 1:55 PM
 */

namespace Tests\Smorken\Error\unit;

use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Application\App;
use Smorken\Error\ErrorHandler;
use Smorken\Http\NotFoundHttpException;
use Smorken\Redactor\Factory;
use Smorken\Redactor\Types\Exception;
use Smorken\Redactor\Types\RegEx;

class ErrorHandlerTest extends TestCase
{

    public function tearDown()
    {
        parent::tearDown();
        m::close();
    }

    public function testHandlerReportsAttemptsToSanitizePw()
    {
        $e = new \Exception('Exception!');
        list($sut, $l, $a, $c) = $this->getSut();
        $c->shouldReceive('get')
          ->with('app.debug')
          ->andReturn(true);
        $c->shouldReceive('get')
          ->with('view')
          ->andReturn(['errors' => 'errors']);
        $a->shouldReceive('offsetGet')
          ->with('view')
          ->andReturn(null);
        $l->shouldReceive('error')
          ->once()
          ->with('Exception', m::type('array'));
        $_REQUEST['my_password'] = 'foo123';
        ob_start();
        $sut->handleException($e);
        $out = ob_get_clean();
        $this->assertContains('my_password: [ REDACTED ]', $out);
        $this->assertNotContains('foo123', $out);
    }

    public function testHandlerReportsWithDebuggingOn()
    {
        $e = new \Exception('Exception!');
        list($sut, $l, $a, $c) = $this->getSut();
        $c->shouldReceive('get')
          ->with('app.debug')
          ->andReturn(true);
        $c->shouldReceive('get')
          ->with('view')
          ->andReturn(['errors' => 'errors']);
        $a->shouldReceive('offsetGet')
          ->with('view')
          ->andReturn(null);
        $l->shouldReceive('error')
          ->once()
          ->with('Exception', m::type('array'));
        ob_start();
        $sut->handleException($e);
        $out = ob_get_clean();
        $this->assertContains('getCode: 0', $out);
        $this->assertNotcontains('getStatus:', $out);
        $this->assertContains('getMessage: Exception!', $out);
        $this->assertContains('session:', $out);
    }

    public function testHandlerSkipsDontReportForLoggingWithDebuggingOffAndView()
    {
        $e = new NotFoundHttpException('Not found');
        list($sut, $l, $a, $c) = $this->getSut();
        $v = m::mock('view');
        $c->shouldReceive('get')
          ->with('app.debug')
          ->andReturn(false);
        $c->shouldReceive('get')
          ->with('view')
          ->andReturn(['errors' => 'errors']);
        $a->shouldReceive('offsetGet')
          ->with('view')
          ->andReturn($v);
        $l->shouldReceive('error')
          ->never();
        $v->shouldReceive('render')
          ->once()
          ->with('errors', m::type('array'))
          ->andReturn('error view');
        $this->assertEquals('error view', $sut->handleException($e));
    }

    public function testHandlerSkipsDontReportForLoggingWithDebuggingOffModifiesMessage()
    {
        $e = new NotFoundHttpException('Nothing found here');
        list($sut, $l, $a, $c) = $this->getSut();
        $c->shouldReceive('get')
          ->with('app.debug')
          ->andReturn(false);
        $c->shouldReceive('get')
          ->with('view')
          ->andReturn(['errors' => 'errors']);
        $a->shouldReceive('offsetGet')
          ->with('view')
          ->andReturn(null);
        $l->shouldReceive('error')
          ->never();
        ob_start();
        $sut->handleException($e);
        $out = ob_get_clean();
        $this->assertNotContains('Nothing found here', $out);
        $this->assertContains('404 - The resource you requested could not be found', $out);
    }

    public function testHandlerSkipsDontReportForLoggingWithDebuggingOn()
    {
        $e = new NotFoundHttpException('Not found');
        list($sut, $l, $a, $c) = $this->getSut();
        $c->shouldReceive('get')
          ->with('app.debug')
          ->andReturn(true);
        $c->shouldReceive('get')
          ->with('view')
          ->andReturn(['errors' => 'errors']);
        $a->shouldReceive('offsetGet')
          ->with('view')
          ->andReturn(null);
        $l->shouldReceive('error')
          ->never();
        ob_start();
        $sut->handleException($e);
        $out = ob_get_clean();
        $this->assertContains('exception: Smorken\Http\NotFoundHttpException', $out);
        $this->assertContains('getMessage: Not found', $out);
        $this->assertNotContains('session:', $out);
    }

    protected function getRedactor(array $config = [])
    {
        if (empty($config)) {
            $config = [
                'active' => true,
                'types'  => [
                    RegEx::class     => [
                        [
                            [
                                '/.*pass.*/i',
                                '/"pass(.*?)"\s*\:\s*"(.*?)"/i',
                                '/.*key.*/i',
                                '/.*salt.*/i',
                                '/.*token.*/i',
                            ],
                        ],
                    ],
                    Exception::class => [
                        [
                            '*::authenticate',
                            [1],
                        ],
                        [
                            '*::connect',
                            [],
                        ],
                    ],
                ],
            ];
        }
        return Factory::fromConfig($config);
    }

    protected function getSut()
    {
        $l = m::mock('Logger');
        $a = m::mock(App::class);
        $c = m::mock('config');
        $a->shouldReceive('offsetGet')
          ->with('config')
          ->andReturn($c);
        $sut = new ErrorHandler($l, $this->getRedactor());
        $sut->setApp($a);
        return [$sut, $l, $a, $c];
    }
}
