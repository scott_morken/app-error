<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 7/24/14
 * Time: 9:05 AM
 */

namespace Smorken\Error;

use Smorken\Redactor\Factory;
use Smorken\Service\Service;

class ErrorService extends Service
{

    public function load()
    {
        $this->bindRedactor();
        $this->app->instance($this->getName(), function ($c) {
            $logger = $c['logger.errors'];
            $redactor = $c['redactor'];
            return new ErrorHandler($logger, $redactor);
        });
    }

    public function start()
    {
        $this->name = 'errors';
    }

    protected function bindRedactor()
    {
        $app = $this->app;
        $this->app->bind('redactor', function ($c) use ($app) {
            $config = $app['config']->get('redactor', []);
            return Factory::fromConfig($config);
        });
    }
} 
