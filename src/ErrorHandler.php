<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 7/23/14
 * Time: 3:12 PM
 */

namespace Smorken\Error;

use Smorken\Application\App;
use Smorken\Http\ForbiddenHttpException;
use Smorken\Http\NotAuthorizedHttpException;
use Smorken\Http\NotFoundHttpException;

class ErrorHandler
{

    protected $logger;

    /**
     * @var \Smorken\Redactor\Contracts\Redactor
     */
    protected $redactor;

    protected $app;

    protected $dontReport = [
        \Smorken\Http\NotAuthorizedHttpException::class,
        \Smorken\Http\NotFoundHttpException::class,
        \Smorken\Http\ForbiddenHttpException::class,
    ];

    public function __construct($logger, $redactor)
    {
        $this->logger = $logger;
        $this->redactor = $redactor;
        set_error_handler([$this, 'handleError']);
        set_exception_handler([$this, 'handleException']);
        register_shutdown_function([$this, 'handleShutdown']);
    }

    /**
     * Handle a PHP error for the application.
     *
     * @param  int    $level
     * @param  string $message
     * @param  string $file
     * @param  int    $line
     * @param  array  $context
     *
     * @return
     * @throws \ErrorException
     */
    public function handleError($level, $message, $file = '', $line = 0, $context = [])
    {
        $error = error_get_last();
        if (!is_null($error)) {
            extract($error);
            if ($this->isFatal($type)) {
                if (error_reporting() & $level) {
                    return $this->handleException(new \ErrorException($message, 0, $level, $file, $line));
                }
            }
        }
    }

    public function handleException($e)
    {
        $data = [];
        switch ($e) {
            case (in_array(get_class($e), $this->dontReport)):
                break;
            default:
                try {
                    $e = $this->sanitize($e);
                    $data['session'] = $this->sanitize($_SESSION);
                    $data['request'] = $this->sanitize($_REQUEST);
                    $data['server'] = $this->sanitize($_SERVER);
                    $this->logger->error($e, $data);
                } catch (\Exception $e2) {
                    //continue if error can't be logged
                }
                break;
        }
        return $this->displayException($data, $e);
    }

    /**
     * Handle the PHP shutdown event.
     *
     * @return
     */
    public function handleShutdown()
    {
        $error = error_get_last();

        // If an error has occurred that has not been displayed, we will create a fatal
        // error exception instance and pass it into the regular exception handling
        // code so it can be displayed back out to the developer for information.
        if (!is_null($error)) {
            extract($error);

            if (!$this->isFatal($type)) {
                return;
            }

            return $this->handleException(new \ErrorException($type . '::' . $message, 0, $type, $file, $line));
        }
    }

    protected function displayException($data, $e)
    {
        $debug = $this->getApp()['config']->get('app.debug');
        $errors = [];
        if ($debug || (defined('DEBUG') && DEBUG)) {
            $errors['exception'] = $this->exceptionToArray($e);
            $errors += $data;
        } else {
            switch ($e) {
                case ($e instanceof NotFoundHttpException):
                    $errors[] = '404 - The resource you requested could not be found.';
                    break;
                case ($e instanceof ForbiddenHttpException):
                    $errors[] = '403 - You do not have access to the requested resource.';
                    break;
                case ($e instanceof NotAuthorizedHttpException):
                    $errors[] = '401 - You must login to access the requested resource.';
                    break;
                default:
                    $errors[] = 'The server has experienced an error.  We will look into it as soon as possible.';
                    break;
            }
        }
        $viewconfig = $this->getApp()['config']->get('view');
        $view = ($viewconfig && isset($viewconfig['errors']) ? $viewconfig['errors'] : 'errors');
        $viewhandler = $this->getApp()['view'];
        if ($viewhandler) {
            try {
                return $viewhandler->render($view, ['errors' => $errors]);
            } catch (\Exception $e) {
                $this->displayRaw($errors);
            }
        } else {
            $this->displayRaw($errors);
        }
    }

    protected function displayRaw($errors)
    {
        printf('<pre>%s</pre>', a2str($errors));
    }

    protected function exceptionToArray($e)
    {
        $methods = [
            'getCode',
            'getFile',
            'getLine',
            'getMessage',
            'getTraceAsString',
        ];
        $data = ['exception' => get_class($e)];
        foreach ($methods as $m) {
            try {
                $data[$m] = $e->$m();
            } catch (\Exception $sube) {
                $data[$m] = 'Failed';
            }
        }
        return $data;
    }

    protected function getApp()
    {
        if (!$this->app) {
            $this->app = \Smorken\Application\App::getInstance();
        }
        return $this->app;
    }

    public function setApp(App $app)
    {
        $this->app = $app;
    }

    /**
     * Determine if the error type is fatal.
     *
     * @param  int $type
     * @return bool
     */
    protected function isFatal($type)
    {
        return in_array($type, [E_ERROR, E_CORE_ERROR, E_COMPILE_ERROR, E_PARSE]);
    }

    protected function sanitize($data)
    {
        if ($this->redactor) {
            return $this->redactor->findAndRedact($data);
        }
        return $data;
    }
}
